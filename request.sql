create table users(
	id serial primary key,
	login varchar unique not null,
	name_u varchar not null,
	role_u varchar
);

create table games(
	id serial primary key,
	player_one_id integer not null,
	player_two_id integer not null,
	result_g varchar,
	foreign key (player_one_id) references users (id),
	foreign key (player_two_id) references users (id)
);

create table turns(
	id serial primary key,
	game_id integer not null,
	order_t integer not null,
	position_t varchar,
	foreign key (game_id) references games (id)
);

insert into users (id, login, name_u, role_u)
values (1, 'one','Alex', 'user_role'),
	   (2, 'two','Bob', 'user_role'),
	   (3, 'three','Jane', 'user_role'),
	   (4, 'four', 'Mike', 'user_role'),
	   (5, 'five', 'Wilson', 'admin_role');

insert into games (id, player_one_id, player_two_id, result_g)
values (1, 1, 2, '1 win'),
	   (2, 3, 4, '3 win'),
	   (3, 5, 1, '1 win'),
	   (4, 4, 1, '1 win'),
	   (5, 3, 2, '2 win'),
	   (6, 1, 2, 'draw');

insert into turns (id, game_id, order_t, position_t)
values (1, 1, 1, 1),
	   (2, 1, 2, 2),
	   (3, 1, 3, 3),
	   (4, 1, 4, 4),
	   (5, 1, 5, 5),
	   (6, 2, 6, 6),
	   (7, 2, 1, 1),
	   (8, 2, 2, 2),
	   (9, 2, 3, 3),
	   (10, 3, 4, 4),
	   (11, 3, 5, 5),
	   (12, 3, 6, 6);

select u.login, u.name_u, g.result_g
from users u
left join games g on g.player_one_id = u.id or g.player_two_id = u.id
where login = 'one' and result_g = '1 win';

select u.login, u.name_u, g.result_g
from users u
left join games g on g.player_one_id = u.id or g.player_two_id = u.id
where login = 'one' and result_g <> '1 win' and result_g <> 'draw';

select u.login, u.name_u, (select count(g.result_g) AS количество_побед from games g where g.result_g = '1 win' and u.login = 'one'), 
(select count(g.result_g) AS количество_ничьих from games g where g.result_g = 'draw' and u.login = 'one'), 
(select count(g.result_g) AS количество_поражений from games g where g.result_g <> 'draw' and g.result_g <> '1 win' and u.login = 'one') 
from users u 
 join games g on g.player_one_id = u.id or g.player_two_id = u.id 
group by u.id, g.result_g 
order by u.id 
limit 1;

select u.login, u.name_u, g.result_g
from users u
left join games g on g.player_one_id = u.id or g.player_two_id = u.id
where result_g LIKE '%win'
order by result_g desc
fetch first 5 rows only;

ALTER TABLE turns
DROP COLUMN id;
